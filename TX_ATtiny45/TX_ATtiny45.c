/*
 * TX_ATtiny45.c
 *
 * Created: 6/10/2015 18:34:24
 *  Author: Brandy
 */ 
#define F_CPU 1000000UL
#include <avr/io.h>
#include <avr/interrupt.h>

#ifndef DBG_PUTCHAR_H
#define DBG_PUTCHAR_H

#include <stdint.h>

/* User setting -> Whether to enable the software UART */
#define DBG_UART_ENABLE		1

/* User setting -> Output pin the software UART */
#define DBG_UART_TX_PORT	PORTB
#define DBG_UART_TX_DDR		DDRB
#define DBG_UART_TX_PIN		PB2

/* User setting -> Software UART baudrate. */
#define DBG_UART_BAUDRATE	9600


/* ---- DO NOT TOUCH BELOW THIS LINE ---- */

#if DBG_UART_ENABLE

/**
 * @brief Debug software UART initialization.
 */
#define dbg_tx_init()	do { \
		DBG_UART_TX_PORT |= (1<<DBG_UART_TX_PIN); \
		DBG_UART_TX_DDR  |= (1<<DBG_UART_TX_PIN); \
	} while(0)

/**
 * @brief Software UART routine for transmitting debug information. 
 * 
 * @warning This routine blocks all interrupts until all 10 bits  ( 1 START +
 * 8 DATA + 1 STOP) are transmitted. This would be about 1ms with 9600bps.
 * 
 * @note Calculation for the number of CPU cycles, executed for one bit time:
 * F_CPU/BAUDRATE = (3*1+2) + (2*1 + (NDLY-1)*4 + 2+1) + 6*1
 * 
 * (NDLY-1)*4 = F_CPU/BAUDRATE - 16
 * NDLY = (F_CPU/BAUDRATE-16)/4+1
 */
extern void dbg_putchar(uint8_t c);

#else
  #define dbg_tx_init()		
  #define dbg_putchar(A)		((void)(A))
#endif	/* DBG_UART_ENABLE */

#endif	/* DBG_PUTCHAR_H */

/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42, by Joerg Wunsch):
 * <dinuxbg .at. gmail.com> wrote this file.  As long as you retain this notice
 * you can do whatever you want with this stuff. If we meet some day, and you 
 * think this stuff is worth it, you can buy me a beer in return.
 *                                                             Dimitar Dimitrov
 * ----------------------------------------------------------------------------
 */

#include <stdint.h>
#include <avr/io.h>
#include <avr/interrupt.h>

#if DBG_UART_ENABLE

void dbg_putchar(uint8_t c)
{
#define DBG_UART_TX_NUM_DELAY_CYCLES	((F_CPU/DBG_UART_BAUDRATE-16)/4+1)
#define DBG_UART_TX_NUM_ADD_NOP		((F_CPU/DBG_UART_BAUDRATE-16)%4)
	uint8_t sreg;
	uint16_t tmp;
	uint8_t numiter = 10;

	sreg = SREG;
	cli();

	asm volatile (
		/* put the START bit */
		"in %A0, %3"		"\n\t"	/* 1 */
		"cbr %A0, %4"		"\n\t"	/* 1 */
		"out %3, %A0"		"\n\t"	/* 1 */
		/* compensate for the delay induced by the loop for the
		 * other bits */
		"nop"			"\n\t"	/* 1 */
		"nop"			"\n\t"	/* 1 */
		"nop"			"\n\t"	/* 1 */
		"nop"			"\n\t"	/* 1 */
		"nop"			"\n\t"	/* 1 */

		/* delay */
	   "1:" "ldi %A0, lo8(%5)"	"\n\t"	/* 1 */
		"ldi %B0, hi8(%5)"	"\n\t"	/* 1 */
	   "2:" "sbiw %A0, 1"		"\n\t"	/* 2 */
		"brne 2b"		"\n\t"	/* 1 if EQ, 2 if NEQ */
#if DBG_UART_TX_NUM_ADD_NOP > 0
		"nop"			"\n\t"	/* 1 */
  #if DBG_UART_TX_NUM_ADD_NOP > 1
		"nop"			"\n\t"	/* 1 */
    #if DBG_UART_TX_NUM_ADD_NOP > 2
		"nop"			"\n\t"	/* 1 */
    #endif
  #endif
#endif
		/* put data or stop bit */
		"in %A0, %3"		"\n\t"	/* 1 */
		"sbrc %1, 0"		"\n\t"	/* 1 if false,2 otherwise */
		"sbr %A0, %4"		"\n\t"	/* 1 */
		"sbrs %1, 0"		"\n\t"	/* 1 if false,2 otherwise */
		"cbr %A0, %4"		"\n\t"	/* 1 */
		"out %3, %A0"		"\n\t"	/* 1 */

		/* shift data, putting a stop bit at the empty location */
		"sec"			"\n\t"	/* 1 */
		"ror %1"		"\n\t"	/* 1 */

		/* loop 10 times */
		"dec %2"		"\n\t"	/* 1 */
		"brne 1b"		"\n\t"	/* 1 if EQ, 2 if NEQ */
		: "=&w" (tmp),			/* scratch register */
		  "=r" (c),			/* we modify the data byte */
		  "=r" (numiter)		/* we modify number of iter.*/
		: "I" (_SFR_IO_ADDR(DBG_UART_TX_PORT)),
		  "M" (1<<DBG_UART_TX_PIN),
		  "i" (DBG_UART_TX_NUM_DELAY_CYCLES),
		  "1" (c),			/* data */
		  "2" (numiter)
	);
	SREG = sreg;
}
#undef DBG_UART_TX_NUM_DELAY_CYCLES
#undef DBG_UART_TX_NUM_ADD_NOP

#endif


int main(void)
{
	char c = 'B';
	char high = 'A';
	char low = 'A';
	char value=0;
	dbg_tx_init();
	// set clock divider to /1
	CLKPR = (1 << CLKPCE);
	CLKPR = (0 << CLKPS3) | (0 << CLKPS2) | (0 << CLKPS1) | (0 << CLKPS0);
	// init A/D
	ADMUX = (0 << REFS2) | (0 << REFS1) | (0 << REFS0) // Vcc ref
	| (0 << ADLAR) // right adjust
	| (0 << MUX3) | (0 << MUX2) | (1 << MUX1) | (1 << MUX0); // ADC3
	ADCSRA = (1 << ADEN) // enable
	| (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // prescaler /128
	// main loop
	while(1) {
		dbg_putchar(c);
		if(++c > 'Z') c = 'A';
		ADCSRA |= (1 << ADSC);
		// wait for completion
		while (ADCSRA & (1 << ADSC));
		// send result
		high = ADCL;
		low = ADCH;
		value = 256*high + low;
		dbg_putchar(value);
	}
	return 0;
}